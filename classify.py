import json

import numpy as np
import pandas as pd
import seaborn as sn
from itertools import combinations

from sklearn.decomposition import PCA
from sklearn.preprocessing import LabelEncoder
from sklearn import neighbors, preprocessing
from sklearn.ensemble import RandomForestClassifier

from random_forest_classifier import rf_classifier
from random_forest_tuning import tune_parameters
from util import extract_paintings, load_data, get_accuracy
from KNN_classifier import knn_classifier_func
from KNN_tuning import tune_parameters_knn
from os import path
from sklearn.metrics import confusion_matrix
from sklearn.linear_model import LogisticRegression
import matplotlib.pyplot as plt

# Load config
with open('etc/config.json', encoding='utf-8') as fh:
    config_file = json.load(fh)

art_df_train = pd.read_csv('results/all_style_contour_train.csv').sample(frac=1)
art_df_test = pd.read_csv('results/all_style_contour_test.csv').sample(frac=1)

# art_df_train=art_df_train.groupby('paint_style', as_index=False).apply(lambda array: array.loc[np.random.choice(array.index, 500, False),:])
# art_df_test=art_df_test.groupby('paint_style', as_index=False).apply(lambda array: array.loc[np.random.choice(array.index, 100, False),:])

# Select styles
if (config_file['selected_styles'] != -1):
    art_df_train = art_df_train[art_df_train['paint_style'].isin(config_file['selected_styles'])]
    art_df_test = art_df_test[art_df_test['paint_style'].isin(config_file['selected_styles'])]

# Encode style as integer
style_encoder = LabelEncoder()
style_encoder.fit(art_df_train['paint_style'].astype(str))
art_df_train['paint_style'] = style_encoder.transform(art_df_train['paint_style'].astype(str))
art_df_test['paint_style'] = style_encoder.transform(art_df_test['paint_style'].astype(str))
selected_styles = style_encoder.transform(config_file['selected_styles'])

# Drop features which are not relevant and scale using min-max
exclude_contour = list(range(-4, -16, -1))
selected_features = []

# Set to [] for all features
if (config_file['contour_features'] == "false"):
    selected_features = exclude_contour


# Exclude filename and row number...
selected_features.extend([-1, 0, 1])

features_extracted_train = art_df_train.drop(art_df_train.columns[selected_features], axis=1)
features_extracted_test = art_df_test.drop(art_df_test.columns[selected_features], axis=1)
scaler = preprocessing.MinMaxScaler()
scaler.fit(features_extracted_train)
features_extracted_train = scaler.transform(features_extracted_train)
features_extracted_test = scaler.transform(features_extracted_test)

style_encoded_train = art_df_train.iloc[:, -1]
style_encoded_test = art_df_test.iloc[:, -1]

style_combinations = combinations(selected_styles, config_file['combination_size']) 

accuracies_list = []

for combination in style_combinations:
    curr_train = features_extracted_train[art_df_train['paint_style'].isin(combination)]
    curr_test = features_extracted_test[art_df_test['paint_style'].isin(combination)]

    curr_out_train = style_encoded_train[art_df_train['paint_style'].isin(combination)]
    curr_out_test = style_encoded_test[art_df_test['paint_style'].isin(combination)]

    tuned_parameters_rf = {'n_estimators': 200, 'min_samples_split': 2, 'min_samples_leaf': 2, 'max_features': 'auto', 'max_depth': 90, 'bootstrap': True}
    #tuned_parameters_rf = tune_parameters(curr_train, curr_out_train, 4)
    clf = RandomForestClassifier(**tuned_parameters_rf)
    clf.fit(curr_train, curr_out_train)
    accuracies_rf, c_matrices_rf = get_accuracy(clf, combination, curr_train, curr_test, curr_out_train, curr_out_test)
    #print(c_matrix_lr)
    print("RF Accuracy with is: " + str(accuracies_rf[1]))

    for i in range(2):
        if i == 0:
            case = 'train'
            confusion_matrix = c_matrices_rf[0]
        else:
            case = 'test'
            confusion_matrix = c_matrices_rf[1]
        df_cm = pd.DataFrame(confusion_matrix, style_encoder.inverse_transform(combination), style_encoder.inverse_transform(combination))
        # plt.figure(figsize=(10,7))
        sn.set(font_scale=1) # for label size
        sn.heatmap(df_cm, annot=True, annot_kws={"size": 8},  cmap='Blues', fmt='d') # font size

        plt.tight_layout()
        plt.savefig('results/c_matrices/' + case + '/conf_matrix_'+ str(style_encoder.inverse_transform(combination)) +'_rf_acc_'+str(accuracies_rf[1])+'.png')
        plt.close()


    LR_model_1 = LogisticRegression(solver='liblinear', max_iter=400)
    LR_model_1.fit(curr_train, curr_out_train)

    accuracies_lr, c_matrices_lr = get_accuracy(LR_model_1, combination, curr_train, curr_test, curr_out_train, curr_out_test)
    #print(c_matrix_lr)
    print("LR Accuracy with is: " + str(accuracies_lr[1]))

    for i in range(2):
        if i == 0:
            case = 'train'
            confusion_matrix = c_matrices_lr[0]
        else:
            case = 'test'
            confusion_matrix = c_matrices_lr[1]
        df_cm = pd.DataFrame(confusion_matrix, style_encoder.inverse_transform(combination), style_encoder.inverse_transform(combination))
        # plt.figure(figsize=(10,7))
        sn.set(font_scale=1) # for label size
        sn.heatmap(df_cm, annot=True, annot_kws={"size": 8},  cmap='Blues', fmt='d') # font size

        plt.tight_layout()
        plt.savefig('results/c_matrices/' + case + '/conf_matrix_'+ str(style_encoder.inverse_transform(combination)) +'_lr_acc_'+str(accuracies_lr[1])+'.png')
        plt.close()

    #####KNN
    tuned_parameters_knn = {'n_neighbors': 100,
                   'weights': 'distance',
                   'algorithm': 'auto',
                   'leaf_size': 30,
                   'p': 2}

    # Scikit-learn implementation with found parameters
    knn_model_scikit = neighbors.KNeighborsClassifier(**tuned_parameters_knn)
    #fit trained datas
    knn_model_scikit.fit(curr_train, curr_out_train)

    accuracies_knn, c_matrices_knn = get_accuracy(knn_model_scikit,combination, curr_train, curr_test, curr_out_train, curr_out_test)
    print("KNN Accuracy with is: " + str(accuracies_knn[1]))
    for i in range(2):
        if i == 0:
            case = 'train'
            confusion_matrix = c_matrices_lr[0]
        else:
            case = 'test'
            confusion_matrix = c_matrices_lr[1]
        df_cm = pd.DataFrame(confusion_matrix, style_encoder.inverse_transform(combination), style_encoder.inverse_transform(combination))
        # plt.figure(figsize=(10,7))
        sn.set(font_scale=1) # for label size
        sn.heatmap(df_cm, annot=True, annot_kws={"size": 8},  cmap='Blues', fmt='d') # font size

        plt.tight_layout()
        plt.savefig('results/c_matrices/' + case + '/conf_matrix_'+ str(style_encoder.inverse_transform(combination)) +'_knn_acc_'+str(accuracies_knn[1])+'.png')
        plt.close()
    
    accuracy = []
    accuracy.append(style_encoder.inverse_transform(combination))
    accuracy.extend(accuracies_rf)
    accuracy.extend(accuracies_knn)
    accuracy.extend(accuracies_lr)

    accuracies_list.append(accuracy)



columns_df = ['style_combination']

for i in ['rf', 'lr', 'knn']:
    for j in ['train', 'test']:
        columns_df.append('accuracy_' + i + '_' + j)

pd.DataFrame(accuracies_list, columns=columns_df).to_csv('results_nocontour_noneoplast.csv')
