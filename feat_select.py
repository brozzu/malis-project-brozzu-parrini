import pandas as pd
import numpy as np
from sklearn.feature_selection import SelectKBest
from sklearn.feature_selection import chi2
from sklearn.preprocessing import LabelEncoder
from sklearn import preprocessing

data = pd.read_csv("results/all_style_contour_train.csv")
style_encoder = LabelEncoder()
style_encoder.fit(data['paint_style'].astype(str))
data['paint_style'] = style_encoder.transform(data['paint_style'].astype(str))

X = data.iloc[:,2:-1]  #independent columns
y = data.iloc[:,-1]    #target column i.e price range

scaler = preprocessing.MinMaxScaler()
scaler.fit(X)
X = scaler.transform(X)

#apply SelectKBest class to extract top 10 best features
bestfeatures = SelectKBest(score_func=chi2, k=10)
fit = bestfeatures.fit(X,y)
dfscores = pd.DataFrame(fit.scores_)
dfcolumns = pd.DataFrame(data.iloc[:,2:-1].columns)
#concat two dataframes for better visualization 
featureScores = pd.concat([dfcolumns,dfscores],axis=1)
featureScores.columns = ['Specs','Score']  #naming the dataframe columns
print(featureScores.nlargest(33,'Score'))  #print 10 best features
