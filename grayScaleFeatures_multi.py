from skimage.filters import gabor
from skimage.color import rgb2gray
from sklearn import preprocessing
from matplotlib.pyplot import imread, imsave
from os import path
from util import progressbar
from perceptual.filterbank import Steerable
from time import sleep
import multiprocessing as mp
import numpy as np

def pyramid_worker(filename, image_root, pyramid_length):
    print("Starting worker " + filename)
    filepath = path.join(image_root, filename)
    extracted_features= np.zeros(len(coeff_pyramid)*2)
    try: 
        img = rgb2gray(imread(filepath))     
    except:
        print("failed to read " + filename)
        return extracted_features
    img = preprocessing.scale(img)
    pyramid_filter = Steerable(pyramid_length)
    coeff_pyramid = pyramid_filter.buildSCFpyr(img)
    for j in range(0, len(coeff_pyramid)*2, 2):
        extracted_features[j] = np.mean(np.abs(coeff_pyramid[int(j/2)]))
        extracted_features[j+1] = np.std(np.abs(coeff_pyramid[int(j/2)]))
    print("Worker finished " + filename)
    return extracted_features

# Pyramid features identyify the intensity of brushes
def extract_pyramid_features(art_df, image_root, pyramid_length):
    # Allow multiprocessing since the computation are independent (should be good for multi-core!)
    num_core = mp.cpu_count()
    pool = mp.Pool(num_core)

    extracted_features = [pool.apply(pyramid_worker, args=(filename, image_root, pyramid_length)) for filename in art_df[::]['new_filename']]
    # Step 3: Don't forget to close
    pool.close() 

    return extracted_features