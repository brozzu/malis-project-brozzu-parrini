## Artstyle recognition through machine learning ##

Authors: Francesco BROZZU, Luca PARRINI

The artistic style (or artistic movement) of a painting is a fundamental element that is used to capture the visual and historical context of a piece of art. Being able to correctly identify an artistic style is a fundamental task when analyzing large digital collections of artwork. 
The aim of the project is to utilize a feature extractor along with a classifier to solve the problem of identifying the artistic style of a painting.
