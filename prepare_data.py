"""Prepare data extracts n_patches per sample of size patch_size (decided in config file) and creates a CSV file
with two columns one for style and the other for the filename
"""

from math import floor
import pandas as pd
from skimage.color import gray2rgb
from matplotlib.pyplot import imread
import matplotlib.pyplot as plt
from os import path
import json
from util import extract_paintings, extract_patches
from sklearn.preprocessing import normalize

# Load config
with open('etc/config.json', encoding='utf-8') as fh:
    config_file = json.load(fh)

N = int(config_file['n_paintings_per_class'])
test_frac = float(config_file['test_frac'])
art_df_full = pd.read_csv(config_file['train_set_full'])

art_df_train, art_df_test = extract_paintings(art_df_full, N, test_frac)

patch_df_train = extract_patches(art_df_train, config_file['image_folder'], config_file['patch_folder'], config_file['patch_size'], config_file['n_patches'])
patch_df_test  = extract_patches(art_df_test, config_file['image_folder'], config_file['patch_folder'], config_file['patch_size'], config_file['n_patches'])

patch_df_train.to_csv('patch_train_' + str(config_file['patch_size']) + '.csv')
patch_df_test.to_csv('patch_test_' + str(config_file['patch_size']) + '.csv')