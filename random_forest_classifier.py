# Classify using the parameters obtained with tuning
import json
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import confusion_matrix
import numpy as np


def rf_classifier(features_extracted_train, style_encoded_train, features_extracted_test, style_encoded_test, cm_labels,
                  rf_params):
    clf = RandomForestClassifier(**rf_params)
    clf.fit(features_extracted_train, style_encoded_train)
    predicted_class = clf.predict(features_extracted_test)
    c_matrix = confusion_matrix(predicted_class, style_encoded_test)
    y_hat_train = clf.predict(features_extracted_train)
    y_hat = clf.predict(features_extracted_test)
    #accuracy
    accuracy_val = np.sum(y_hat == style_encoded_test) / y_hat.size
    accuracy_train = np.sum(y_hat_train == style_encoded_train) / y_hat_train.size
    c_matrix = confusion_matrix(y_hat, style_encoded_test, labels=cm_labels)

    return accuracy_train, accuracy_val, c_matrix
