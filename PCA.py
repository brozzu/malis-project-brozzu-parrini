import json

import numpy as np
import pandas as pd
from sklearn.decomposition import PCA
from sklearn.preprocessing import LabelEncoder

from random_forest_classifier import rf_classifier
from random_forest_tuning import tune_parameters
from util import extract_paintings, load_data
from KNN_classifier import knn_classifier_func
from KNN_tuning import tune_parameters_knn
from os import path
import seaborn as sn
import matplotlib.pyplot as plt
from util import progressbar


# Load config
with open('etc/config.json', encoding='utf-8') as fh:
    config_file = json.load(fh)

N = int(config_file['n_paintings_per_class'])
image_sizes = [64]
test_frac = float(config_file['test_frac'])
art_df_full = pd.read_csv(config_file['train_set_full'])
art_df_full = art_df_full.loc[art_df_full['in_train'] == False, ::]

# Check balance
# art_df_full['paint_style'].value_counts().plot(kind='bar')
# plt.show()

[art_df_train, art_df_test] = extract_paintings(art_df_full, N, test_frac)

image_root = config_file['image_folder']

n_components = np.arange(2, 120, 10)

accuracies_train_rf = []
accuracies_train_knn = []

accuracies_val_rf = []
accuracies_val_knn = []

parameters_rf = []
parameters_knn = []

# Encode labels
style_encoder = LabelEncoder()
style_encoder.fit(art_df_train['paint_style'].astype(str))
art_df_train['paint_style'] = style_encoder.transform(art_df_train['paint_style'].astype(str))
art_df_test['paint_style'] = style_encoder.transform(art_df_test['paint_style'].astype(str))


# Loop over different image sizes
for image_size in image_sizes:
    # Load normalized training and test
    paintings_train, style_encoded_train = load_data(art_df_train, path.join(image_root, str(image_size)))
    paintings_test, style_encoded_test = load_data(art_df_test, path.join(image_root, str(image_size)))

    n_styles = np.unique(style_encoded_train).shape[0]

    # Test different n_components
    for components in progressbar(n_components, "Components: ", 40):  
        paintings_pca = PCA(n_components=components)
        features_extracted_train = paintings_pca.fit_transform(paintings_train)
        # Extract features from test with obtained model
        features_extracted_test = paintings_pca.transform(paintings_test)

        #random_forest_classifier
        #tuned_parameters_rf = tune_parameters(features_extracted_train, style_encoded_train, config_file['n_processes'])
        tuned_parameters_rf = {'n_estimators': 2000, 'min_samples_split': 2, 'min_samples_leaf': 2, 'max_features': 'auto', 'max_depth': 90, 'bootstrap': True}
        accuracy_rf_train, accuracy_rf_val, cm_rf = rf_classifier(features_extracted_train, style_encoded_train, features_extracted_test, style_encoded_test, np.arange(n_styles), tuned_parameters_rf)

        df_cm = pd.DataFrame(cm_rf, style_encoder.inverse_transform(range(n_styles)), style_encoder.inverse_transform(range(n_styles)))
        # plt.figure(figsize=(10,7))
        sn.set(font_scale=1) # for label size
        sn.heatmap(df_cm, annot=True, annot_kws={"size": 10},  cmap='Blues') # font size
        plt.savefig('results/c_matrices_PCA/c_matrix_pca_components' + str(components) + '_rf.eps', bbox_inches='tight', dpi=100)
        plt.clf()

        tuned_parameters_knn = tune_parameters_knn(features_extracted_train, style_encoded_train)
        tuned_parameters_knn = random_grid = {'n_neighbors': 50,
                   'weights': 'distance',
                   'algorithm': 'auto',
                   'leaf_size': 30,
                   'p': 2}

        accuracy_knn_train, accuracy_knn_val, cm_knn = knn_classifier_func(features_extracted_train, style_encoded_train, features_extracted_test, style_encoded_test, np.arange(n_styles),tuned_parameters_knn)

        df_cm = pd.DataFrame(cm_knn, style_encoder.inverse_transform(range(n_styles)), style_encoder.inverse_transform(range(n_styles)))
        # plt.figure(figsize=(10,7))
        sn.set(font_scale=1) # for label size
        sn.heatmap(df_cm, annot=True, annot_kws={"size": 10},  cmap='Blues') # font size
        plt.savefig('results/c_matrices_PCA/c_matrix_pca_components' + str(components) + '_knn.eps', bbox_inches='tight', dpi=100)
        plt.clf()

        accuracies_train_rf.append(accuracy_rf_train)
        accuracies_train_knn.append(accuracy_knn_train)

        accuracies_val_rf.append(accuracy_rf_val)
        accuracies_val_knn.append(accuracy_knn_val)

        parameters_rf.append(tuned_parameters_rf)
        parameters_knn.append(tuned_parameters_knn)
        print("Image size: " + str(image_size))
        print("RF Accuracy with " + str(components) + " components is: " + str(accuracy_rf_val))
        print("KNN Accuacy with " + str(components) + " components is: " + str(accuracy_knn_val))


    np.save('results/accuracies_val_size_'+ str(image_size) +'_nfig' + str(N) + '.npy', [n_components, accuracies_val_knn, accuracies_val_rf])
    np.save('results/accuracies_train_size_'+ str(image_size) +'_nfig' + str(N) + '.npy', [n_components, accuracies_train_knn, accuracies_train_rf])
    np.save('results/parameters_rf_knn_'+ str(image_size) + '_nfig' + str(N) + '.npy', [parameters_rf, parameters_knn])


