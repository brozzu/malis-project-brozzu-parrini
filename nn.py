"""" First neural network test """

import torch
import pandas as pd
import json
from sklearn.preprocessing import LabelEncoder

with open('etc/config.json', encoding='utf-8') as fh:
    config_file = json.load(fh)

dtype = torch.float
device = torch.device("cuda:0")

# N is batch size; D_in is input dimension;
# H is hidden dimension; D_out is output dimension.
N, D_in, H, D_out = 64, 1000, 100, 10

w1 = torch.randn(D_in, H, device=device, dtype=dtype, requires_grad=True)
w2 = torch.randn(H, D_out, device=device, dtype=dtype, requires_grad=True)

learning_rate = 1e-6

art_df_train = pd.read_csv('gray_features_train_fullimage.csv')
art_df_test = pd.read_csv('gray_features_test_fullimage.csv')

# Select styles
if (config_file['selected_styles'] != -1):
    art_df_train = art_df_train[art_df_train['paint_style'].isin(config_file['selected_styles'])]
    art_df_test = art_df_test[art_df_test['paint_style'].isin(config_file['selected_styles'])]

style_encoder = LabelEncoder()
style_encoder.fit(art_df_train['paint_style'].astype(str))
art_df_train['paint_style'] = style_encoder.transform(art_df_train['paint_style'].astype(str))
art_df_test['paint_style'] = style_encoder.transform(art_df_test['paint_style'].astype(str))

x = art_df_train.iloc[:, 1:-1]
features_extracted_test = art_df_test.iloc[:, 1:-1]
y = art_df_train.iloc[:, -1]
style_encoded_test = art_df_test.iloc[:, -1]

model = torch.nn.Sequential(
    torch.nn.Linear(D_in, H),
    torch.nn.ReLU(),
    torch.nn.Linear(H, D_out),
)