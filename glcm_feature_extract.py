import json
import pandas as pd
from grayScaleFeatures import extract_pyramid_features
from hsv_features import extract_hsv_features
from contours import extract_contours_features
from os import path
from util import extract_glcm_features, extract_paintings
import numpy as np


def extract_features(df, config_file, filename):
    print("Computing pyramid features...")
    pyramid_features = extract_pyramid_features(
        df, config_file['image_folder'], config_file['pyramid_order'])
    print("Computing color features...")
    hsv_features = extract_hsv_features(df, config_file['image_folder'])
    print("Computing GLCM features...")
    contrast, energy = extract_glcm_features(df, config_file['image_folder'])
    print("Computing contour features")
    contour_features = extract_contours_features(
        df, config_file['image_folder'])
    filename_list = np.array(df['new_filename'], dtype=str).reshape(-1, 1)

    columns = ['filename']
    color_parameters = ['hue_mean',
                    'saturation_mean',
                    'maxval_mean',
                    'hue_variance',
                    'saturation_variance',
                    'maxval_variance',
                    'hue_skew',
                    'saturation_skew',
                    'maxval_skew',
                    'hue_kurtosis',
                    'saturation_kurtosis',
                    'maxval_kurtosis']
    contour_parameters = []
    for i in ['result', 'delta', 'ratio', 'num_lines']:
        for j in ['canny', 'adaptive', 'norm']:
            contour_parameters.append(i + '_' + j)

    for i in range(config_file['pyramid_order']+1):
        columns.append('pyramid_coeff_mean_' + str(i))
        columns.append('pyramid_coeff_std_' + str(i))
    columns.extend(color_parameters)
    columns.extend(contour_parameters)
    columns.extend(['contrast',
                    'energy',
                    'paint_style'])

    
    pd.DataFrame(np.concatenate((filename_list, pyramid_features, hsv_features, contour_features, contrast,
                                 energy, np.array(df['paint_style'], dtype=str).reshape(-1, 1)), axis=1), columns=columns).to_csv(filename)


# Load config
with open('etc/config.json', encoding='utf-8') as fh:
    config_file = json.load(fh)

#save_glcm_features('patch_train_128.csv', 'patch_test_128.csv', config_file)

art_df_full = pd.read_csv(config_file['train_set_full'])

if (config_file['n_paintings_per_class'] == -1):
    extract_features(art_df_full, config_file, 'results/testing_new.csv')

else:
    print("Extracting selected styles...")
    art_df_train, art_df_test = extract_paintings(art_df_full, int(
        config_file['n_paintings_per_class']), float(config_file['test_frac']))

    if (config_file['selected_styles'] != -1):
        art_df_train = art_df_train[art_df_train['paint_style'].isin(
            config_file['selected_styles'])]
        art_df_test = art_df_test[art_df_test['paint_style'].isin(
            config_file['selected_styles'])]

    extract_features(art_df_train, config_file,
                     'results/add_style_contour_train.csv')
    extract_features(art_df_test, config_file, 'results/add_style_contour_test.csv')
