from skimage.filters import gabor
from skimage.color import rgb2gray
from sklearn import preprocessing
from matplotlib.pyplot import imread, imsave
from os import path
from util import progressbar
#from perceptual.filterbank import Steerable
import pyrtools as pt
import numpy as np

# Pyramid features identyify the intensity of brushes
def extract_pyramid_features(art_df, image_root, pyr_order):

    extracted_features = np.zeros([len(art_df[::]['new_filename']), (pyr_order+1)*2])
    i = 0
    for filename in progressbar(art_df[::]['new_filename'], "Computing: ", 40):  
        filepath = path.join(image_root, filename)
        try: 
            img = rgb2gray(imread(filepath))     
        except:
            i = i + 1
            continue
        img = preprocessing.scale(img)
        imSubSample = 1
        img = pt.blurDn(img, n_levels=imSubSample, filt='qmf9')
        pyr = pt.pyramids.SteerablePyramidSpace(img, 'auto', order=pyr_order)
        scale = 0
        coeff_pyramid = pyr.pyr_coeffs
        for j in range(0, (pyr_order+1)*2, 2):
            extracted_features[i][j] = np.mean(np.abs(coeff_pyramid[scale, int(j/2)]))
            extracted_features[i][j+1] = np.std(np.abs(coeff_pyramid[scale, int(j/2)]))
        i = i + 1
    return extracted_features
