from math import floor
import pandas as pd
import numpy as np
from skimage.color import gray2rgb
from skimage.color import rgb2gray
from sklearn.feature_extraction import image
from matplotlib.pyplot import imread, imsave
from os import path
from sklearn.preprocessing import normalize
from skimage.feature import greycomatrix, greycoprops
import sys
from sklearn.metrics import confusion_matrix


# Useful if you want to show progress bar in large computations
def progressbar(it, prefix="", size=60, file=sys.stdout):
    count = len(it)
    def show(j):
        x = int(size*j/count)
        file.write("%s[%s%s] %i/%i\r" % (prefix, "#"*x, "."*(size-x), j, count))
        file.flush()        
    show(0)
    for i, item in enumerate(it):
        yield item
        show(i+1)
    file.write("\n")
    file.flush()

def extract_paintings(art_df, n, test_frac):
    # art_df = art_df.loc[art_df['in_train'] == False, ::]

    art_df_train_list = []
    art_df_test_list = []

    counted_paintings = art_df.groupby('paint_style').count()
    counted_paintings = counted_paintings[::]['new_filename']
    counted_paintings_list = counted_paintings.index.tolist()
    counted_paintings_clean = []

    # Get styles that have at least N+0.2*N paintings, so we have a test as well
    for i in range(len(counted_paintings)):
        if counted_paintings[i] > floor(n + test_frac * n):
            counted_paintings_clean.append([counted_paintings_list[i], floor(n + 0.2 * n)])

    for i in range(len(counted_paintings_clean)):
        art_df_train_list.extend(
            art_df.loc[art_df['paint_style'] == counted_paintings_clean[i][0], ::].head(n).values.tolist())
        art_df_test_list.extend(
            art_df.loc[art_df['paint_style'] == counted_paintings_clean[i][0], ::].tail(floor(0.2 * n)).values.tolist())

    # data_types= dict(data_types)

    art_df_train = pd.DataFrame(art_df_train_list,
                                columns=['artist', 'date', 'genre', 'pixelsx', 'pixelsy', 'size_bytes', 'source',
                                         'paint_style', 'title', 'artist_group', 'in_train', 'new_filename'])
    art_df_test = pd.DataFrame(art_df_test_list,
                               columns=['artist', 'date', 'genre', 'pixelsx', 'pixelsy', 'size_bytes', 'source',
                                        'paint_style', 'title', 'artist_group', 'in_train', 'new_filename'])
    return art_df_train, art_df_test

def extract_patches(art_df, image_root, patch_dest, patch_size, n_patches):
    imread.MAX_IMAGE_PIXELS = None
    # Get the unique styles from the provided DataFrame
    selected_styles = art_df.paint_style.unique()
    patch_df_array = []
    # Now let's get them one by one and extract patches
    for style in selected_styles:
        # Select paintings of style we're looping through using loc
        art_df_curr_style = art_df.loc[art_df['paint_style'] == style, ::]
        for filename in art_df_curr_style[::]['new_filename']:
            filepath = path.join(image_root, filename)
            # If there is an error reading the image -> discard!
            try:
                img = imread(filepath)
            except:
                continue
            # If not RGB create "fake channels" in order to always have the same dimension
            if img.ndim < 3:
                img = gray2rgb(img)
            if img.ndim < 3:
                #image is broken discard
                continue
            patches = image.extract_patches_2d(img, (patch_size, patch_size), max_patches=n_patches)
            for patch in range(patches.shape[0]):
                savepath = path.join(patch_dest + '/', str(patch_size) + '/', str(patch) + '_' + filename)
                imsave(savepath, patches[patch, :, :, :])
                patch_df_array.append([str(patch) + '_' + filename, style])
    
    patch_df = pd.DataFrame(patch_df_array, columns=['filename', 'paint_style'])
    return patch_df


def load_patches(art_df, image_root, patch_size, n_patches_sel):
    styles = []
    image_dict = {}
    i = 0

    for style in art_df.paint_style.unique():   
        if n_patches_sel == -1:
            n_patches = art_df_curr_style
        else:   
            n_patches = n_patches_sel
        art_df_curr_style = art_df.loc[art_df['paint_style'] == style, ::].head(n_patches)
        for filename in art_df_curr_style[::]['filename']:
            filepath = path.join(image_root + '/', str(patch_size) + '/', filename)
            img = imread(filepath)
            styles.append(art_df.loc[art_df['filename'] == filename, ::]['paint_style'].item())
            image_dict[i] = img.flatten()
            i = i+1
    image_dict = pd.DataFrame.from_dict(image_dict, "index")
    image_dict = normalize(image_dict)
    return image_dict, styles

def load_data(art_df, image_root):
    style_encoded = []
    image_dict = {}
    i = 0
    for filename in art_df[::]['new_filename']:
        filepath = path.join(image_root, filename)
        img = imread(filepath)
        if img.ndim < 3:
            img = gray2rgb(img)
        flattened_img = img.flatten()
        style_encoded.append(art_df.loc[art_df['new_filename'] == filename, ::]['paint_style'].item())
        image_dict[i] = flattened_img
        i = i + 1
    image_dict = pd.DataFrame.from_dict(image_dict, "index")
    image_dict = normalize(image_dict)
    return image_dict, style_encoded

def extract_glcm_features(art_df, image_root):
    contrast = np.zeros(len(art_df[::]['new_filename']))
    energy = np.zeros(len(art_df[::]['new_filename']))
    i = 0
    for filename in progressbar(art_df[::]['new_filename'], "Computing: ", 40):        
        filepath = path.join(image_root, filename)
        try: 
            img = np.uint8(rgb2gray(imread(filepath)) * 255)   
        except:
            #image is broken discard
            i = i + 1
            continue
        glcm = greycomatrix(img, distances=[5], angles=[0], levels=256,
                    symmetric=True, normed=True)
        contrast[i] = greycoprops(glcm, 'contrast')[0, 0]
        energy[i] = greycoprops(glcm, 'energy')[0, 0]
        i = i + 1
    return np.array(contrast).reshape(-1, 1), np.array(energy).reshape(-1, 1)

def get_accuracy(model, c_labels, input_train, input_test, output_train, output_test):
    accuracies = []
    c_matrices = []
    y_hat = model.predict(input_train)   
    accuracies.append(np.sum(y_hat == output_train)/output_train.size)
    c_matrices.append(confusion_matrix(output_train, y_hat))
    y_hat = model.predict(input_test)   
    accuracies.append(np.sum(y_hat == output_test)/output_test.size)
    c_matrices.append(confusion_matrix(output_test, y_hat, labels=c_labels))
    return accuracies, c_matrices
        

        

# def save_glcm_features(filename_train, filename_test, config_file):

#     art_df_train = pd.read_csv(filename_train)
#     art_df_test = pd.read_csv(filename_test)

#     extract_glcm_features(art_df_train, path.join(config_file['patch_folder'] + '/', str(config_file['patch_size']))).to_csv('glcm_train_' + str(config_file['patch_size']) + '.csv')
#     extract_glcm_features(art_df_test, path.join(config_file['patch_folder'] + '/', str(config_file['patch_size']))).to_csv('glcm_test_' + str(config_file['patch_size']) + '.csv')