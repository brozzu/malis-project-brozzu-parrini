from skimage.filters import gabor
from skimage.color import rgb2hsv
from sklearn import preprocessing
from matplotlib.pyplot import imread, imsave
from os import path
from util import progressbar
from scipy.stats import kurtosis, skew
from perceptual.filterbank import Steerable
import numpy as np

# art df is csv list of paintings
# image root is a string of folder path to the images files
def extract_hsv_features(art_df, image_root):
    extracted_features = np.zeros([len(art_df[::]['new_filename']), 12])
    i = 0
    for filename in progressbar(art_df[::]['new_filename'], "Computing: ", 40):
        filepath = path.join(image_root, filename)
        try:
            img = rgb2hsv(imread(filepath))
        except:
            i = i + 1
            continue
        hue_img = img[:, :, 0]
        sat_img = img[:, :, 1]
        value_img = img[:, :, 2]

        extracted_features[i][0] = np.mean(hue_img)
        extracted_features[i][1] = np.mean(sat_img)
        extracted_features[i][2] = np.mean(value_img)

        extracted_features[i][3] = np.var(hue_img)
        extracted_features[i][4] = np.var(sat_img)
        extracted_features[i][5] = np.var(value_img)

        extracted_features[i][6] = skew(np.ravel(hue_img))
        extracted_features[i][7] = skew(np.ravel(sat_img))
        extracted_features[i][8] = skew(np.ravel(value_img))

        extracted_features[i][9] = kurtosis(np.ravel(hue_img))
        extracted_features[i][10] = kurtosis(np.ravel(sat_img))
        extracted_features[i][11] = kurtosis(np.ravel(value_img))

        # 12 values for each image sample
        i = i + 1
    return extracted_features