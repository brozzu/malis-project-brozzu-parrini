import seaborn as sn
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
from sklearn.preprocessing import LabelEncoder


confusion_matrix = np.load('results/knn_cf_glcm.npy')
art_df_train = pd.read_csv('glcm_train.csv')
art_df_test = pd.read_csv('glcm_test.csv')

style_encoder = LabelEncoder()
style_encoder.fit(art_df_train['paint_style'].astype(str))
art_df_train['paint_style'] = style_encoder.transform(art_df_train['paint_style'].astype(str))
art_df_test['paint_style'] = style_encoder.transform(art_df_test['paint_style'].astype(str))

df_cm = pd.DataFrame(confusion_matrix, style_encoder.inverse_transform(range(confusion_matrix.shape[0])), style_encoder.inverse_transform(range(confusion_matrix.shape[0])))
# plt.figure(figsize=(10,7))
sn.set(font_scale=1) # for label size
sn.heatmap(df_cm, annot=True, annot_kws={"size": 10},  cmap='Blues') # font size

plt.show()
