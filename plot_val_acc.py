import matplotlib.pyplot as plt
import numpy as np

[n_components, accuracies_knn, accuracies_rf] = np.load('results/accuracies_train_size_64_nfig1000.npy')
[n_components, accuracies_knn_val, accuracies_rf_val] = np.load('results/accuracies_val_size_64_nfig1000.npy')


plt.plot(n_components, accuracies_knn*100)
plt.plot(n_components, accuracies_rf*100)
plt.plot(n_components, accuracies_knn_val*100)
plt.plot(n_components, accuracies_rf_val*100)
plt.xlabel('no. of PCA components')
plt.ylabel('Train accuracy %')
plt.legend(['KNN classifier training', 'RF classifier training', 'KNN classifier validation', 'RF classifier validation'])
plt.savefig('results/pca_64_knn_rf.eps')
plt.savefig('results/pca_64_knn_rf.png')
