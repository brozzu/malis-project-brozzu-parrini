import matplotlib.pyplot as plt
from os import path
from util import progressbar
import numpy as np
import cv2 as cv


# art df is csv list of paintings
# image root is a string of folder path to the images files
def extract_contours_features(art_df, image_root):

    extracted_features = np.zeros([len(art_df[::]['new_filename']), 12])
    i = 0
    contrast = 70
    brightness = 50
    value = 0
    for filename in progressbar(art_df[::]['new_filename'], "Computing: ", 40):
        filepath = path.join(image_root, filename)
        try:
            image = cv.imread(filepath)
            image = cv.resize(image, (960, 540))

            img = image_manipulation(image, contrast, brightness, value)
            img = cv.bilateralFilter(img, 9, sigmaColor=200,sigmaSpace=200)
            img=color_quantization(img, 4)
            img = cv.medianBlur(img, 15)

            #if testing needed with hsv
            #hsv = cv.cvtColor(img, cv.COLOR_BGR2HSV)
            #h, s, v = cv.split(hsv)
            #blur = s

            img = cv.cvtColor(img, cv.COLOR_RGB2GRAY)
            blur = cv.equalizeHist(img)
            # #clahe = cv.createCLAHE()
            # #img = clahe.apply(img)

            binary_canny = cv.Canny(blur, cv.THRESH_OTSU * 0.5, cv.THRESH_OTSU)

            result_canny, delta_canny, ratio_canny, num_lines_canny = contour_study(binary_canny)

            _, binary_norm = cv.threshold(blur, 0, 255, cv.THRESH_BINARY | cv.THRESH_OTSU)  # 125

            element = cv.getStructuringElement(cv.MORPH_RECT, (3, 3))
            j = 0
            while j < 2:
                j += 1
                binary_norm = cv.morphologyEx(binary_norm, cv.MORPH_OPEN, element)  # Open and close to make appear contours
                binary_norm = cv.morphologyEx(binary_norm, cv.MORPH_CLOSE, element)

            result_norm, delta_norm, ratio_norm, num_lines_norm = contour_study(binary_norm)

            binary_adaptive = cv.adaptiveThreshold(blur, 255, cv.ADAPTIVE_THRESH_GAUSSIAN_C, cv.THRESH_BINARY, 45, 2)
            element = cv.getStructuringElement(cv.MORPH_RECT, (3, 3))
            j = 0
            while j < 2:
                j += 1
                binary_adaptive = cv.morphologyEx(binary_adaptive, cv.MORPH_OPEN, element)  # Open and close to make appear contours
                binary_adaptive = cv.morphologyEx(binary_adaptive, cv.MORPH_CLOSE, element)

            result_adaptive, delta_adaptive, ratio_adaptive, num_lines_adaptive = contour_study(binary_adaptive)

        except Exception as e:
            print(str(e))
            i = i + 1
            continue

        ############ result ##################
        extracted_features[i][0] = result_canny
        extracted_features[i][1] = result_adaptive
        extracted_features[i][2] = result_norm
        ############ delta ###################
        extracted_features[i][3] = delta_canny
        extracted_features[i][4] = delta_norm
        extracted_features[i][5] = delta_adaptive
        ############ ratio ###################
        extracted_features[i][6] = ratio_canny
        extracted_features[i][7] = ratio_norm
        extracted_features[i][8] = ratio_adaptive
        ############ num_lines ###############
        extracted_features[i][9] = num_lines_canny
        extracted_features[i][10] = num_lines_norm
        extracted_features[i][11] = num_lines_adaptive
        ######################################

        i = i + 1

    return extracted_features

def image_manipulation (image,contrast,brightness,value) :

    # adjust brightness
    if (brightness > 0):
        alpha_c = (255 - brightness) / 255
        gamma_c = brightness
        image = cv.addWeighted(image, alpha_c, image, 0, gamma_c)
    ##############################
    # adjust contrast
    if (contrast != 0):
        f = 131 * (contrast + 127) / (127 * (131 - contrast))
        alpha_c = f
        gamma_c = 127 * (1 - f)
        image = cv.addWeighted(image, alpha_c, image, 0, gamma_c)
    ##############################
    # adjust HUE SAT MAXVAL
    if (value > 0):
        hsv = cv.cvtColor(image, cv.COLOR_RGB2HSV)
        h, s, v = cv.split(hsv)
        h += value  # HUE
        s += value  # SATURATION
        v += value  # MAX VALUE
        final_hsv = cv.merge((h, s, v))
        image = cv.cvtColor(final_hsv, cv.COLOR_HSV2RGB)
    ###############################
    return image

def color_quantization(img, k):
    # Defining input data for clustering
    data = np.float32(img).reshape((-1, 3))
    # Defining criteria
    criteria = (cv.TERM_CRITERIA_EPS + cv.TERM_CRITERIA_MAX_ITER, 20, 1.0)
    # Applying cv2.kmeans function
    ret, label, center = cv.kmeans(data, k, None, criteria, 10, cv.KMEANS_RANDOM_CENTERS)
    center = np.uint8(center)
    result = center[label.flatten()]
    result = result.reshape(img.shape)
    return result

def contour_study(binary, approx_acc = 0.2, line_len_approx = 15):

    contours, hierarchy = cv.findContours(binary, cv.RETR_CCOMP, cv.CHAIN_APPROX_SIMPLE)  # cv.CHAIN_APPROX_NONE)

    contours_app = []
    original = 0
    approximation = 0

    ##############################################################################
    # contours_new=[]
    # for contour in contours:
    #      epsilon = 0.0007 * cv.arcLength(contour, False) #0.024
    #      approx_or = cv.approxPolyDP(contour, epsilon, closed=False)
    #      contours_new.append(approx_or)
    # contours=contours_new
    ################################################################################

    for contour in contours:
        epsilon = approx_acc * cv.arcLength(contour, False)  # 0.024
        if epsilon > line_len_approx:
            epsilon = line_len_approx
        approx = cv.approxPolyDP(contour, epsilon, closed=False)
        contours_app.append(approx)

    new_image = np.zeros((binary.shape[0], binary.shape[1], 3), np.uint8)
    new_image = cv.drawContours(new_image, contours_app, -1, (255, 255, 255), 3)
    new_image = cv.cvtColor(new_image, cv.COLOR_RGB2GRAY)

    new_original_image = np.zeros((binary.shape[0], binary.shape[1], 3), np.uint8)
    new_original_image = cv.drawContours(new_original_image, contours, -1, (255, 255, 255), 3)
    new_original_image = cv.cvtColor(new_original_image, cv.COLOR_RGB2GRAY)

    result = cv.matchShapes(new_image, new_original_image, cv.CONTOURS_MATCH_I3, 0)

    for contour in contours:
        original = original + cv.arcLength(contour, False)
    for contour in contours_app:
        approximation = approximation + cv.arcLength(contour, False)

    delta = (original - approximation) / original
    ratio = approximation / original

    ###########################################################
    lines = cv.HoughLinesP(new_original_image, 1, np.pi / 180, 350, 350)  # cv.HOUGH_PROBABILISTIC
    if lines is not None:
        num_lines = len(lines)
    else:
        num_lines = 0
    ###########################################################

    ######################## print image test #####################
    # binary = cv.drawContours(binary, contours, -1, (0, 255, 0), 2)
    # binary = cv.drawContours(binary, contours_app, -1, (0, 255, 255), 2)
    # binary = cv.resize(binary, (960, 540))  # Resize image
    # cv.imshow('image', binary)
    # cv.waitKey(0)
    ######################## print image test #####################

    return result, delta, ratio, num_lines