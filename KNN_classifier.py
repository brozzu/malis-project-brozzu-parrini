# Classify with knn using the parameters obtained with tuning
import json
from sklearn import neighbors, datasets, model_selection
from sklearn.metrics import confusion_matrix
import numpy as np


def knn_classifier_func(features_extracted_train, style_encoded_train, features_extracted_test, style_encoded_test, cm_labels, tuned_parameters):
    # Scikit-learn implementation with found parameters
    knn_model_scikit = neighbors.KNeighborsClassifier(**tuned_parameters)
    #fit trained datas
    knn_model_scikit.fit(features_extracted_train, style_encoded_train)
    #predict
    y_hat = knn_model_scikit.predict(features_extracted_test)
    y_hat_train = knn_model_scikit.predict(features_extracted_train)
    #accuracy
    accuracy_val = np.sum(y_hat == style_encoded_test) / y_hat.size
    accuracy_train = np.sum(y_hat_train == style_encoded_train) / y_hat_train.size
    c_matrix = confusion_matrix(y_hat, style_encoded_test)#, labels=cm_labels)
    return accuracy_train, accuracy_val, c_matrix