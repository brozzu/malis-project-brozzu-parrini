# Use me to tune the random forest parameter ie. find the best parameters

from pprint import pprint
import numpy as np
from sklearn import neighbors, datasets, model_selection
from sklearn.model_selection import RandomizedSearchCV


def tune_parameters_knn(features_extracted_train, style_encoded_train):
    # Number of elements to search from
    n_neighbors = [int(x) for x in np.linspace(start=1, stop=len(features_extracted_train), num=1)]
    #weight parameter (I think distance is most useful => nearest==+importance)
    weights = ['uniform', 'distance']
    #algorithm type
    algorithm = ['auto', 'ball_tree', 'kd_tree', 'brute']
    #at what point passing to brute force method
    leaf_size = [int(x) for x in np.linspace(start=15, stop=100, num=5)]
    p = [1, 2]

    # n_neighbors:int, default = 5
    # weights={‘uniform’, ‘distance’} or callable, default =’uniform’
    # algorithm:{‘auto’, ‘ball_tree’, ‘kd_tree’, ‘brute’}, default =’auto’
    # leaf_size:int, default = 30
    # p:int, default = 2
    # metric:str or callable, default =’minkowski’
    # metric_params:dict, default = None

    # Create the random grid
    random_grid = {'n_neighbors': n_neighbors,
                   'weights': weights,
                   'algorithm': algorithm,
                   'leaf_size': leaf_size,
                   'p': p}

    # Use the random grid to search for best hyperparameters
    # First create the base model to tune
    knn = neighbors.KNeighborsClassifier()
    # Random search of parameters, using 3 fold cross validation,
    # search across 100 different combinations, and use all available cores
    knn_random = RandomizedSearchCV(estimator=knn, param_distributions=random_grid, n_iter=100, cv=3, verbose=2,
                                   random_state=42, n_jobs=4)
    knn_random.fit(features_extracted_train, style_encoded_train)

    return knn_random.best_params_
