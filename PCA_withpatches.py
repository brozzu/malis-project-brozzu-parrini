import json

import numpy as np
import pandas as pd
from sklearn.decomposition import PCA
from sklearn.preprocessing import LabelEncoder

from random_forest_classifier import rf_classifier
from random_forest_tuning import tune_parameters
from util import extract_paintings, load_patches
from KNN_classifier import knn_classifier_func
from KNN_tuning import tune_parameters_knn
from os import path

# Load config
with open('etc/config.json', encoding='utf-8') as fh:
    config_file = json.load(fh)

N = int(config_file['n_paintings_per_class'])

art_df_train = pd.read_csv('patch_train.csv')
art_df_test = pd.read_csv('patch_test.csv')

n_components = [2, 10, 20, 30, 40]

accuracies_rf = []
accuracies_knn = []
c_matrices_rf = []

parameters_rf = []
parameters_knn = []

# Encode labels
style_encoder = LabelEncoder()
style_encoder.fit(art_df_train['paint_style'].astype(str))
art_df_train['paint_style'] = style_encoder.transform(art_df_train['paint_style'].astype(str))
art_df_test['paint_style'] = style_encoder.transform(art_df_test['paint_style'].astype(str))


# Load normalized training and test, n_patches_sel =-1 for all patches otherwise the specified N 
paintings_train, style_encoded_train = load_patches(art_df_train, config_file['patch_train_folder'], config_file['n_patches_sel'])
paintings_test, style_encoded_test = load_patches(art_df_test, config_file['patch_test_folder'], config_file['n_patches_sel'])
# Test different n_components
for components in n_components:
    paintings_pca = PCA(n_components=components)
    features_extracted_train = paintings_pca.fit_transform(paintings_train)
    # Extract features from test with obtained model
    features_extracted_test = paintings_pca.transform(paintings_test)

    #random_forest_classifier
    tuned_parameters_rf = tune_parameters(features_extracted_train, style_encoded_train, config_file['n_processes'])
    #tuned_parameters_rf = {'n_estimators': 2000, 'min_samples_split': 2, 'min_samples_leaf': 2, 'max_features': 'auto', 'max_depth': 90, 'bootstrap': True}
    accuracy_rf, c_matrix_rf = rf_classifier(features_extracted_train, style_encoded_train, features_extracted_test, style_encoded_test, tuned_parameters_rf)

    parameters_rf.append(tuned_parameters_rf)
    c_matrices_rf.append(c_matrix_rf)
    print("RF Accuracy with " + str(components) + " components is: " + str(accuracy_rf))
    print(c_matrix_rf)


np.save('results/accuracies_patches_'+ config_file['patch_size'] +'.npy', [n_components, accuracies_rf])
np.save('results/cmatrix_patches_'+ config_file['patch_size'] +'.npy', c_matrices_rf)
np.save('results/parameters_patches_rf_knn_'+ config_file['patch_size'] +'.npy', [parameters_rf, parameters_knn])


